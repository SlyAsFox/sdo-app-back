
# SDO App API
> 1.0

API for Covent SDO App. Main functionality: login / registration for users, view other users profiles, personal todo list for each user.

## Requirements

- node `v21.2.0`


## Used in project

- Nest ^10.0.0

## Getting started

Project use cloud service, so .env file must be added to project directory.

```bash
# create .env file 
touch  .env

#Set next variables
NODE_ENV=
PORT=

DB_SERVICE_URI=
DB_NAME=
DB_HOST=
DB_PORT=
DB_USER=
DB_PASSWORD=

JWT_SECRET=
JWT_ACCESS_EXPIRE=
JWT_REFRESH_EXPIRE=

S3_ACCESS_KEY_ID=
S3_SECRET_ACCESS_KEY=/US
S3_REGION=
S3_BUCKET_NAME=

# installing dependencies
npm i

# run app
npm run start:dev   
```

## Usage

Documentation: http://localhost:{{PORT}}/api/docs#/
