export interface AdminJSOptions {
  adminJsOptions: {
    rootPath: string;
    resources: any[];
    branding: {
      companyName: string;
      logo: string;
      favicon: string;
    };
  };
  auth: {
    authenticate: (email: string, password: string) => Promise<any>;
    cookieName: string;
    cookiePassword: string;
  };
  sessionOptions: {
    resave: boolean;
    saveUninitialized: boolean;
    secret: string;
  };
}
