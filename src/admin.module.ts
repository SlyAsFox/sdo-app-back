import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities';
import { Todo } from './todos/entities';

const DEFAULT_ADMIN = {
  email: 'admin',
  password: 'admin',
};

const authenticate = async (email: string, password: string) => {
  if (email === DEFAULT_ADMIN.email && password === DEFAULT_ADMIN.password) {
    return Promise.resolve(DEFAULT_ADMIN);
  }
  return null;
};

@Module({
  imports: [ConfigModule.forRoot(), TypeOrmModule.forFeature([User, Todo])], // додайте Todo тут
  providers: [
    {
      provide: 'ADMIN_JS_OPTIONS',
      useFactory: async (configService: ConfigService) => {
        const { AdminJS } = await import('adminjs');
        const AdminJSTypeorm = await import('@adminjs/typeorm');
        AdminJS.registerAdapter({
          Resource: AdminJSTypeorm.Resource,
          Database: AdminJSTypeorm.Database,
        });

        return {
          adminJsOptions: {
            rootPath: '/admin',
            resources: [
              {
                resource: User,
                options: {
                  listProperties: ['name', 'email', 'createdAt'],
                  sortBy: 'name',
                  direction: 'asc',
                  navigation: {
                    // name: 'User Management',
                    icon: 'Users',
                  },
                },
              },
              {
                resource: Todo, // додайте конфігурацію для Todo
                options: {
                  listProperties: [
                    'text',
                    'userId',
                    'isCompleted',
                    'finishedAt',
                    'createdAt',
                  ],
                  sortBy: 'createdAt',
                  direction: 'desc',
                  navigation: {
                    // name: 'Todo Management',
                    icon: 'List',
                  },
                },
              },
            ],
            branding: {
              companyName: `${configService.get('PROJECT_NAME')} - Admin panel`,
              logo: '/logo.svg',
              favicon: '/favicon-32x32.png',
            },
          },
          auth: {
            authenticate,
            cookieName: 'adminjs',
            cookiePassword: 'secret',
          },
          sessionOptions: {
            resave: true,
            saveUninitialized: true,
            secret: 'secret',
          },
        };
      },
      inject: [ConfigService],
    },
  ],
  exports: ['ADMIN_JS_OPTIONS'],
})
export class AdminModule {}
