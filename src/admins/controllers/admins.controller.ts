import {
  Controller,
  Get,
  Post,
  Param,
  Body,
  Delete,
  Patch,
  Query,
} from '@nestjs/common';
import { AdminService } from '../services';
import { Admin } from '../entities';
import { CreateAdminDto, UpdateAdminDto } from '../dto';
import { CurrentUser } from '../../auth/decorators';
import { BaseQueryParams } from '../../base/dto';
import { Not } from 'typeorm';

@Controller({
  version: '1',
  path: 'users',
})
export class AdminsController {
  constructor(private readonly adminService: AdminService) {}

  @Get()
  async findAll(
    @Query() params: BaseQueryParams,
    @CurrentUser('sub') userId: string,
  ): Promise<Admin[]> {
    return this.adminService.findAll({ id: Not(userId) }, params);
  }

  @Get('profile')
  async getProfile(@CurrentUser('sub') userId: string): Promise<any> {
    return this.adminService.findOne(userId);
  }

  @Patch('profile')
  async updateProfile(
    @CurrentUser('sub') userId: string,
    @Body() updateAdminDto: UpdateAdminDto,
  ): Promise<Admin> {
    return this.adminService.update(userId, updateAdminDto);
  }

  @Get('email/:email')
  async findAdminByUsername(@Param('email') email: string): Promise<Admin> {
    return this.adminService.findUserByUsername(email);
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Admin> {
    return this.adminService.findOne(id);
  }

  @Post()
  async create(@Body() createAdminDto: CreateAdminDto): Promise<Admin> {
    return this.adminService.create(createAdminDto);
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateAdminDto,
  ): Promise<Admin> {
    return this.adminService.update(id, updateUserDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string): Promise<void> {
    return this.adminService.remove(id);
  }
}
