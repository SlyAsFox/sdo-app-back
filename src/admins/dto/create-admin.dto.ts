import {
  IsNotEmpty,
  IsString,
  IsEmail,
  Length,
  IsOptional,
  IsUrl,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateAdminDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  @Length(4, 50)
  name!: string;

  @IsNotEmpty()
  @IsString()
  @IsEmail()
  @ApiProperty()
  email!: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  password!: string;

  @IsOptional()
  @IsString()
  @IsUrl()
  @ApiProperty({ required: false })
  avatarOriginal?: string;

  @IsOptional()
  @IsString()
  @IsUrl()
  @ApiProperty({ required: false })
  avatarPreview?: string;
}
