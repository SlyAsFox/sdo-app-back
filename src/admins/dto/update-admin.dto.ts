import {
  IsOptional,
  IsString,
  IsEmail,
  Length,
  IsUrl,
} from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateAdminDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  @Length(4, 50)
  name!: string;

  @IsOptional()
  @IsString()
  @IsEmail()
  @ApiPropertyOptional({
    example: 'mail@gmail.com',
    description: 'Should be an email',
  })
  email!: string;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  password!: string;

  @IsOptional()
  @IsString()
  @IsUrl()
  @ApiPropertyOptional()
  avatarOriginal?: string;

  @IsOptional()
  @IsString()
  @IsUrl()
  @ApiPropertyOptional()
  avatarPreview?: string;
}
