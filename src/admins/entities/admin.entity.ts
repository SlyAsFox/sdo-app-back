import {
  Entity,
  Column,
  getMetadataArgsStorage,
  BeforeInsert,
  BeforeUpdate,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from '../../base';

@Entity('admins')
export class Admin extends BaseEntity {
  @ApiProperty()
  @Column()
  name: string;

  @ApiProperty()
  @Column({ nullable: true, name: 'avatar_original' })
  avatarOriginal: string;

  @ApiProperty()
  @Column({ nullable: true, name: 'avatar_preview' })
  avatarPreview: string;

  @ApiProperty()
  @Column()
  email: string;

  @ApiProperty()
  @Column({ select: false })
  password: string;

  static getColumnNames(): string[] {
    const columns = getMetadataArgsStorage().columns.filter((column) => {
      return column.target === Admin || column.target === BaseEntity;
    });
    return columns.map((column) => column.propertyName);
  }

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    if (this.password) {
      this.password = bcrypt.hashSync(this.password, 10);
    }
  }
}
