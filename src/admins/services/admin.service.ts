import { Injectable } from '@nestjs/common';
import { Admin } from '../entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BaseService } from '../../base';

@Injectable()
export class AdminService extends BaseService<Admin> {
  constructor(
    @InjectRepository(Admin)
    private readonly adminRepository: Repository<Admin>,
  ) {
    super(adminRepository);
  }

  async findUserByUsername(
    username: string,
    opts?: {
      password?: boolean;
    },
  ): Promise<Admin | null> {
    const entity = await this.adminRepository.findOne({
      where: {
        email: username,
      },
      select: opts?.password
        ? (Admin.getColumnNames() as (keyof Admin)[])
        : undefined,
    });

    return entity || null;
  }
}
