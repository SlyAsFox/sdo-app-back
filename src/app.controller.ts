import { Controller, Get } from '@nestjs/common';

@Controller()
export class AppController {
  constructor() {}

  @Get('healthy')
  healthyCheck(): string {
    return 'Ok';
  }
}
