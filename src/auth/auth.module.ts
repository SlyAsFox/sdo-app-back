import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { options } from './config/jwt-module-async-options';
import { STRATEGIES } from './strategies';
import { UserAuthController, AdminAuthController } from './controllers';
import { AuthService } from './services';
import { UsersModule } from '../users/users.module';
import { RefreshToken } from './entities';
import { AdminsModule } from '../admins/admins.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([RefreshToken]),
    UsersModule,
    AdminsModule,
    JwtModule.registerAsync(options()),
    PassportModule,
  ],
  controllers: [UserAuthController, AdminAuthController],
  providers: [...STRATEGIES, AuthService],
  exports: [AuthService],
})
export class AuthModule {}
