import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from '../services';
import { LoginDto } from '../dto';
import { TokensPair } from '../interfaces';
import { Public } from '../decorators';
import { TokensPairResponse } from '../responses/token-pair.response';
import {
  ApiOperation,
  ApiResponse,
  ApiBadRequestResponse,
  ApiTags,
  ApiBody,
} from '@nestjs/swagger';
import { UserType } from '../datasets';
import { RefreshTokensDto } from '../dto';

@Public()
@ApiTags('Admin Authentication')
@Controller('auth/admin')
export class AdminAuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: 'Login admin' })
  @ApiResponse({
    status: 200,
    description: 'Admin logged in successfully',
    type: TokensPairResponse,
  })
  @ApiBadRequestResponse({ description: 'Invalid credentials' })
  @Post('login')
  async login(@Body() body: LoginDto): Promise<TokensPair> {
    return this.authService.adminLogin(body);
  }

  @ApiOperation({ summary: 'Refresh access token' })
  @ApiResponse({
    status: 200,
    description: 'Access token refreshed successfully',
    type: TokensPairResponse,
  })
  @ApiBody({ type: RefreshTokensDto })
  @ApiBadRequestResponse({ description: 'Invalid refresh token' })
  @Post('refresh')
  async refresh(@Body() body: { refreshToken: string }): Promise<TokensPair> {
    return this.authService.refresh(body.refreshToken, UserType.ADMIN);
  }
}
