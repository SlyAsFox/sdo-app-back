import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from '../services';
import { LoginDto } from '../dto';
import { TokensPair, TokensPairWithUser } from "../interfaces";
import { Public } from '../decorators';
import { TokensPairResponse } from '../responses/token-pair.response';
import {
  ApiOperation,
  ApiResponse,
  ApiBadRequestResponse,
  ApiTags,
  ApiBody,
} from '@nestjs/swagger';
import { UserType } from '../datasets';
import { UserSignupDto, RefreshTokensDto } from '../dto';

@Public()
@ApiTags('User Authentication')
@Controller({
  version: '1',
  path: 'auth/user',
})
export class UserAuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: 'Login user' })
  @ApiBody({ type: LoginDto })
  @ApiResponse({
    status: 200,
    description: 'User logged in successfully',
    type: TokensPairResponse,
  })
  @ApiBadRequestResponse({ description: 'Invalid credentials' })
  @Post('login')
  async login(@Body() body: LoginDto): Promise<TokensPair> {
    return this.authService.userLogin(body);
  }

  @ApiOperation({ summary: 'Signup user' })
  @ApiBody({ type: UserSignupDto })
  @ApiResponse({
    status: 201,
    description: 'User registered successfully',
    type: TokensPairResponse,
  })
  @ApiBadRequestResponse({ description: 'Invalid user data' })
  @Post('signup')
  async signup(@Body() body: UserSignupDto): Promise<TokensPairWithUser> {
    return this.authService.userSignup(body);
  }

  @ApiOperation({ summary: 'Refresh access token' })
  @ApiResponse({
    status: 200,
    description: 'Access token refreshed successfully',
    type: TokensPairResponse,
  })
  @ApiBadRequestResponse({ description: 'Invalid refresh token' })
  @ApiBody({ type: RefreshTokensDto })
  @Post('refresh')
  async refresh(
    @Body('refreshToken') refreshToken: string,
  ): Promise<TokensPair> {
    return this.authService.refresh(refreshToken, UserType.USER);
  }
}
