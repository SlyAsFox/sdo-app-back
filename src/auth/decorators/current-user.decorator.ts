import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { accessJwtPayload as jwtPayload } from '../interfaces';

// Use only in methods with jwt
export const CurrentUser = createParamDecorator(
  (
    key: keyof jwtPayload,
    ctx: ExecutionContext,
  ): jwtPayload | Partial<jwtPayload> | null => {
    const request = ctx.switchToHttp().getRequest();
    try {
      return key ? request.user.payload[key] : request.user.payload;
    } catch {
      return null;
    }
  },
);
