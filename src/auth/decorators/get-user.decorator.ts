import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { accessJwtPayload as jwtPayload } from '../interfaces';
import { User } from '../../users/entities';

// Use only in methods with jwt
export const GetUser = createParamDecorator(
  (
    key: keyof User,
    ctx: ExecutionContext,
  ): jwtPayload | Partial<jwtPayload> | null => {
    const request = ctx.switchToHttp().getRequest();
    try {
      return key ? request.user.user[key] : request.user.user;
    } catch {
      return null;
    }
  },
);
