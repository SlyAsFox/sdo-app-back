import { Entity, Column } from 'typeorm';
import { BaseEntity } from '../../base';
import { ApiProperty } from '@nestjs/swagger';

@Entity('refresh_tokens')
export class RefreshToken extends BaseEntity {
  @ApiProperty()
  @Column({ name: 'token_id' })
  tokenId: string;

  @ApiProperty()
  @Column({ name: 'user_id' })
  userId: string;
}
