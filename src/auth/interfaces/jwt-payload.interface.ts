import { TokenType, UserType } from '../datasets';

export interface jwtPayload {
  type: TokenType;
}

export interface accessJwtPayload extends jwtPayload {
  userType: UserType;
  sub: string;
}

export interface refreshJwtPayload extends jwtPayload {
  id: string;
}
