export interface TokensPair {
  accessToken: string;
  refreshToken: string;
}

export interface TokensPairWithUser {
  accessToken: string;
  refreshToken: string;
  user: any;
}


