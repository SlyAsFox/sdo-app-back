import { ApiProperty } from '@nestjs/swagger';

export class TokensPairResponse {
  @ApiProperty()
  accessToken!: string;

  @ApiProperty()
  refreshToken!: string;
}
