import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { compareSync } from 'bcrypt';
import { v4 } from 'uuid';
import { TokenType, UserType } from '../datasets';
import {
  accessJwtPayload,
  refreshJwtPayload,
  TokensPair,
  TokensPairWithUser,
} from '../interfaces';
import { UserSignupDto, LoginDto } from '../dto';
import { UsersService } from '../../users/services';
import { RefreshToken } from '../entities';
import { AdminService } from '../../admins/services';
import { CustomErrorException, ErrorCodes } from "../../base";

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(RefreshToken)
    private readonly refreshTokenRepository: Repository<RefreshToken>,
    private userService: UsersService,
    private adminService: AdminService,
    private jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  private generateAccessToken = (
    userId: string,
    userType: UserType,
  ): string => {
    const payload: accessJwtPayload = {
      sub: userId,
      userType,
      type: TokenType.ACCESS,
    };
    return this.jwtService.sign(payload, {
      expiresIn: this.configService.get('JWT_ACCESS_EXPIRE'),
    });
  };

  private generateRefreshToken = (): {
    id: string;
    token: string;
  } => {
    const payload: refreshJwtPayload = {
      id: v4(),
      type: TokenType.REFRESH,
    };
    const options = {
      expiresIn: this.configService.get('JWT_REFRESH_EXPIRE'),
    };

    return {
      id: payload.id,
      token: this.jwtService.sign(payload, options),
    };
  };

  private replaceRefreshTokenFromStorage = async (
    tokenId: string,
    userId: string,
  ): Promise<void> => {
    await this.refreshTokenRepository.delete({
      userId,
    });

    await this.refreshTokenRepository.create({ id: v4(), tokenId, userId });
  };

  private updateTokens = async (
    userId: string,
    userType: UserType,
  ): Promise<TokensPair> => {
    const accessToken = this.generateAccessToken(userId, userType);
    const refreshToken = this.generateRefreshToken();

    await this.replaceRefreshTokenFromStorage(refreshToken.id, userId);

    return {
      accessToken,
      refreshToken: refreshToken.token,
    };
  };

  async userLogin(data: LoginDto): Promise<TokensPairWithUser> {
    const user = await this.userService.findUserByUsername(data.email, {
      password: true,
    });

    if (!user) {
      throw new CustomErrorException(ErrorCodes.USER_NOT_FOUND);
    }
    if (!user.password) {
      throw new CustomErrorException(ErrorCodes.PASSWORD_IS_NULL);
    }

    if (!compareSync(data.password, user.password)) {
      throw new CustomErrorException(ErrorCodes.INVALID_USERNAME_OR_PASSWORD);
    }
    const tokens = await this.updateTokens(user.id, UserType.USER);
    return {
      ...tokens,
      user,
    };
  }

  async userSignup(createUserDto: UserSignupDto): Promise<TokensPairWithUser> {
    const existUsername = await this.userService.findUserByUsername(
      createUserDto.email,
    );
    if (existUsername) {
      throw new CustomErrorException(ErrorCodes.USER_ALREADY_REGISTERED);
    }

    const user = await this.userService.create({
      email: createUserDto.email,
      password: createUserDto.password,
      name: createUserDto.name,
    });

    const tokens = await this.updateTokens(user.id, UserType.USER);

    return {
      ...tokens,
      user,
    };
  }

  async refresh(refreshToken: string, userType: UserType): Promise<TokensPair> {
    let payload: refreshJwtPayload;
    try {
      payload = this.jwtService.verify(refreshToken);
    } catch (error) {
      throw new CustomErrorException(ErrorCodes.UNAUTHORIZED);
    }
    if (payload.type !== TokenType.REFRESH) {
      throw new CustomErrorException(ErrorCodes.UNAUTHORIZED);
    }
    const existRefreshToken = await this.refreshTokenRepository.findOne({
      where: {
        tokenId: payload.id,
      },
    });

    if (!existRefreshToken) {
      throw new CustomErrorException(ErrorCodes.UNAUTHORIZED);
    }
    return this.updateTokens(existRefreshToken.userId, userType);
  }

  async adminLogin(data: LoginDto): Promise<TokensPair> {
    const admin = await this.adminService.findUserByUsername(data.email);

    if (!admin || !compareSync(data.password, admin.password)) {
      throw new CustomErrorException(ErrorCodes.INVALID_USERNAME_OR_PASSWORD);
    }

    return this.updateTokens(admin.id, UserType.ADMIN);
  }
}
