import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { accessJwtPayload } from '../interfaces';
import { ConfigService } from '@nestjs/config';
import { TokenType, UserType } from '../datasets';
import { UsersService } from '../../users/services';
import { AdminService } from '../../admins/services';
import { CustomErrorException, ErrorCodes } from '../../base';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly configService: ConfigService,
    private readonly userService: UsersService,
    private readonly adminService: AdminService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get('JWT_SECRET'),
    });
  }

  async validate(payload: accessJwtPayload) {
    if (payload.type !== TokenType.ACCESS) {
      throw new CustomErrorException(ErrorCodes.UNAUTHORIZED);
    }
    const useService =
      payload.userType === UserType.ADMIN
        ? this.adminService
        : this.userService;

    let user = null;
    try {
      user = await useService.findOne(payload.sub);
      if (!user || payload.type !== TokenType.ACCESS) {
        throw new CustomErrorException(ErrorCodes.UNAUTHORIZED);
      }
    } catch (error) {
      throw new CustomErrorException(ErrorCodes.UNAUTHORIZED);
    }

    return { payload, user };
  }
}
