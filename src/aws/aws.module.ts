import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { S3V3Service } from './services';
import { FilesController } from './controllers';

@Module({
  imports: [ConfigModule],
  controllers: [FilesController],
  providers: [S3V3Service],
  exports: [S3V3Service],
})
export class AwsModule {}
