import {
  Controller,
  Post,
  Get,
  Param,
  UseInterceptors,
  Res,
  UploadedFiles,
  Req,
  Delete,
  NestInterceptor,
} from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { Response } from 'express';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { FileUploadDto } from '../dto/upload-file.dto';
import { S3V3Service } from '../services';
import { ConfigService } from '@nestjs/config';
import { Public } from '../../auth/decorators';

@Controller({
  version: '1',
  path: 'files',
})
@ApiBearerAuth('Authorization')
@ApiTags('Files')
export class FilesController {
  constructor(
    private readonly s3Service: S3V3Service,
    private configService: ConfigService,
  ) {}

  @Public()
  @Post('upload')
  @UseInterceptors(
    FileFieldsInterceptor([
      { name: 'original', maxCount: 1 },
      { name: 'preview', maxCount: 1 },
    ]),
  )
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: FileUploadDto })
  async uploadFile(
    @UploadedFiles() files: any,
    @Req() request: any,
  ): Promise<any> {
    const host = request.get('host');
    const originalUrl = request.originalUrl;

    const originalFile = files?.original?.pop();
    const previewFile = files?.preview?.pop();
    let originalFileId = '';
    let previewFileId = '';

    if (originalFile) {
      originalFileId = await this.s3Service.uploadFile(
        originalFile.buffer,
        originalFile.originalname,
      );
    }
    if (previewFile) {
      previewFileId = await this.s3Service.uploadFile(
        previewFile.buffer,
        previewFile.originalname,
      );
    }
    return {
      original: `http://${host}${originalUrl}`
        .replace('upload', originalFileId)
        .replace('dev/', '')
        .replace('stage/', '')
        .replace('prod/', ''),
      preview: previewFileId
        ? `http://${host}${originalUrl}`
            .replace('upload', previewFileId)
            .replace('dev/', '')
            .replace('stage/', '')
            .replace('prod/', '')
        : null,
    };
  }

  @Public()
  @Get(':id')
  @ApiParam({ name: 'id', description: 'The ID of the file to retrieve' })
  @ApiResponse({
    status: 200,
    description: 'Returns the requested file.',
    schema: {
      type: 'string',
      format: 'binary',
    },
  })
  async getFile(@Param('id') id: string, @Res() res: Response): Promise<any> {
    const result = await this.s3Service.getFile(id);

    res.setHeader('Content-Type', result.contentType);
    res.setHeader('Content-Disposition', `attachment; filename=${id}`);

    // Send the file buffer
    res.send(result.buffer);
  }

  @Delete(':id')
  @ApiParam({ name: 'id', description: 'The ID of the file to deelte' })
  @ApiResponse({
    status: 200,
    description: 'Delete file by id.',
  })
  async deleteFile(@Param('id') id: string): Promise<void> {
    return this.s3Service.deleteFile(id);
  }
}
