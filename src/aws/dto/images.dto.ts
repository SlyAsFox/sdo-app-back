import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ImageDto {

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  itemId!: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  originalUrl!: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  previewUrl!: string;
  
}
