import { IsBoolean, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResponseTrueDto {

  @IsNotEmpty()
  @IsBoolean()
  @ApiProperty()
  data!: boolean;
  
}
