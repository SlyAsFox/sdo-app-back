import { ApiProperty } from '@nestjs/swagger';

export class FileUploadDto {
  @ApiProperty({ type: 'string', format: 'binary', required: true })
  original: any;

  @ApiProperty({ type: 'string', format: 'binary', required: false })
  preview?: any;
}
