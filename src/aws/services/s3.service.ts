import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { v4 as uuidv4 } from 'uuid';
import {
  S3Client,
  GetObjectCommand,
  DeleteObjectCommand,
} from '@aws-sdk/client-s3';
import { Upload } from '@aws-sdk/lib-storage';
import { Readable } from 'stream';
import { CustomErrorException, ErrorCodes } from '../../base';

@Injectable()
export class S3V3Service {
  private s3: S3Client;
  private readonly maxFileSize = 1 * 1024 * 1024; // 1 MB in bytes
  private readonly bucketName: string;

  constructor(private configService: ConfigService) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.s3 = new S3Client({
      region: this.configService.get('S3_REGION'),
      credentials: {
        accessKeyId: this.configService.get('S3_ACCESS_KEY_ID'),
        secretAccessKey: this.configService.get('S3_SECRET_ACCESS_KEY'),
      },
    });

    this.bucketName = this.configService.get('S3_BUCKET_NAME');
  }

  private getContentType(filename: string): string {
    const extension = filename.split('.').pop()?.toLowerCase();

    const mimeTypes: { [key: string]: string } = {
      png: 'image/png',
      jpg: 'image/jpeg',
      jpeg: 'image/jpeg',
      gif: 'image/gif',
      bmp: 'image/bmp',
      webp: 'image/webp',
    };

    return mimeTypes[extension] || 'application/octet-stream';
  }

  async uploadFile(fileBuffer: Buffer, filename: string): Promise<string> {
    if (fileBuffer.length > this.maxFileSize) {
      throw new CustomErrorException(ErrorCodes.FILE_SIZE_LIMIT);
    }

    const uniqueFilename = this.generateUniqueFilename(filename);
    const contentType = this.getContentType(filename);

    const upload = new Upload({
      client: this.s3,
      params: {
        Bucket: this.bucketName,
        Key: `${this.configService.get('NODE_ENV')}/${uniqueFilename}`,
        Body: fileBuffer,
        ContentType: contentType,
      },
    });

    const uploadResult = await upload.done();
    return uploadResult.Key;
  }

  private generateUniqueFilename(originalFilename: string): string {
    const extension = originalFilename.split('.').pop();
    return `${uuidv4()}.${extension}`;
  }

  async deleteFile(key: string): Promise<void> {
    const params = {
      Bucket: this.bucketName,
      Key: key,
    };

    await this.s3.send(new DeleteObjectCommand(params));
  }

  async getFile(key: string): Promise<{
    contentType: string;
    buffer: Buffer;
  }> {
    const params = {
      Bucket: this.bucketName,
      Key: `${this.configService.get('NODE_ENV')}/${key}`,
    };

    const command = new GetObjectCommand(params);
    const { ContentType, Body } = await this.s3.send(command);

    if (!Body) {
      throw new CustomErrorException(ErrorCodes.IMAGE_NOT_FOUND);
    }

    // Cast Body to the correct type for Node.js environment
    return {
      contentType: ContentType,
      buffer: await this.streamToBuffer(Body as Readable),
    };
  }

  private async streamToBuffer(stream: Readable): Promise<Buffer> {
    const chunks: Buffer[] = [];
    for await (const chunk of stream) {
      chunks.push(chunk);
    }
    return Buffer.concat(chunks);
  }
}
