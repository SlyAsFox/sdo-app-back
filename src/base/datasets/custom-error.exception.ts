import { BadRequestException } from '@nestjs/common';
import { ErrorCodes } from './error-codes.enum';
import { ErrorMessages } from './error-messages.enum';

export class CustomErrorException extends BadRequestException {
  constructor(
    public readonly errorCode: ErrorCodes,
    public readonly customMessage?: string,
  ) {
    const message =
      customMessage ||
      ErrorMessages[errorCode]?.message ||
      'An unexpected error occurred';

    super(message, errorCode);
  }
}
