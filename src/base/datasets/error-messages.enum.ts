import { HttpStatus } from '@nestjs/common';

export interface ErrorMessage {
  message: string;
  code: HttpStatus;
}

export const ErrorMessages: Record<string, ErrorMessage> = {
  USER_NOT_FOUND: {
    message: 'User not found.',
    code: HttpStatus.NOT_FOUND,
  },
  USER_ALREADY_REGISTERED: {
    message: 'User already registered.',
    code: HttpStatus.CONFLICT,
  },
  PASSWORD_IS_NULL: {
    message: 'User without password. Reset password before try to change.',
    code: HttpStatus.CONFLICT,
  },
  VALIDATION_ERROR: {
    message: 'Validation error occurred.',
    code: HttpStatus.UNPROCESSABLE_ENTITY,
  },
  UNAUTHORIZED: {
    message: 'Unauthorized access.',
    code: HttpStatus.UNAUTHORIZED,
  },
  SOMETHING_WENT_WRONG: {
    message: 'An unexpected error occurred.',
    code: HttpStatus.INTERNAL_SERVER_ERROR,
  },
  EMAIL_NOT_FOUND: {
    message: 'Email not found.',
    code: HttpStatus.NOT_FOUND,
  },
  INVALID_USERNAME_OR_PASSWORD: {
    message: 'Invalid username or password',
    code: HttpStatus.NOT_FOUND,
  },
  FILE_SIZE_LIMIT: {
    message: 'File size exceeds the 1 MB limit.',
    code: HttpStatus.BAD_REQUEST,
  },
  IMAGE_NOT_FOUND: {
    message: 'Image with provided id not found.',
    code: HttpStatus.NOT_FOUND,
  },
};
