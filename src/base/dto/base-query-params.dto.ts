import { Transform } from 'class-transformer';
import {
  IsInt,
  IsOptional,
  IsPositive,
  IsString,
  Min,
  Length,
} from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class BaseQueryParams {
  @ApiPropertyOptional({ description: 'Search term for filtering users' })
  @IsOptional()
  @IsString()
  @Length(0, 100)
  search?: string;

  @ApiPropertyOptional({
    description: 'Maximum number of users to retrieve',
    minimum: 1,
  })
  @IsOptional()
  @IsInt()
  @IsPositive()
  @Min(1)
  @Transform(({ value }) => parseInt(value, 10))
  limit?: number;

  @ApiPropertyOptional({
    description: 'Offset for user retrieval, used for pagination',
    minimum: 0,
  })
  @IsOptional()
  @IsInt()
  @Min(0)
  @Transform(({ value }) => parseInt(value, 10))
  offset?: number;
}
