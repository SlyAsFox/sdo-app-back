export interface ListWithCount<T> {
  data: T[];
  count: number;
}
