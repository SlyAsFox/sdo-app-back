import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  BadRequestException,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { CustomErrorException, ErrorCodes, ErrorMessages } from '../datasets';

@Catch(HttpException)
export class AllExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const exceptionResponse = exception.getResponse() as
      | { message: string; error: string }
      | string;

    const { statusCode, errorCode, message } = this.getErrorDetails(
      exception,
      exceptionResponse,
    );

    response.status(statusCode).json({
      statusCode,
      errorCode,
      message,
      timestamp: new Date().toISOString(),
      path: request.url,
    });
  }

  private toUpperSnakeCase(str) {
    return str.replace(/([a-z])([A-Z])/g, '$1_$2').toUpperCase();
  }

  private getErrorDetails(
    exception: HttpException,
    exceptionResponse: { message: string; error: string } | string,
  ) {
    let statusCode: number = 500;
    let errorCode: ErrorCodes = ErrorCodes.SOMETHING_WENT_WRONG;
    let message: string = ErrorMessages.SOMETHING_WENT_WRONG.message;

    if (
      exception instanceof BadRequestException
    ) {
      statusCode = ErrorMessages.VALIDATION_ERROR.code;
      errorCode = ErrorCodes.VALIDATION_ERROR;
      message =
        typeof exceptionResponse === 'string'
          ? exceptionResponse
          : exceptionResponse.message || ErrorMessages.VALIDATION_ERROR.message;
    } else if (exception instanceof CustomErrorException) {
      const { message: exceptionMessage, error } =
        typeof exceptionResponse === 'string'
          ? { message: exceptionResponse, error: exceptionResponse }
          : exceptionResponse;

      errorCode = Object.values(ErrorCodes).includes(error as ErrorCodes)
        ? (error as ErrorCodes)
        : ErrorCodes.SOMETHING_WENT_WRONG;
      statusCode = ErrorMessages[errorCode]?.code || exception.getStatus();
      message = ErrorMessages[errorCode]?.message || exceptionMessage;
    } else if (exception instanceof HttpException) {
      errorCode = this.toUpperSnakeCase(exception.name) as ErrorCodes;
      statusCode = exception.getStatus();
      message = exception.message;
    }

    return { statusCode, errorCode, message };
  }
}
