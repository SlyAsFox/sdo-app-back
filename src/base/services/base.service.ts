import {
  Repository,
  DeepPartial,
  FindOptionsWhere,
  FindManyOptions,
  FindOptionsOrder,
} from 'typeorm';
import { BaseEntity } from '../entities';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { BaseQueryParams } from '../dto';

export abstract class BaseService<T extends BaseEntity> {
  protected constructor(private readonly repository: Repository<T>) {}

  async findAll(
    filter?: FindOptionsWhere<T>,
    params?: BaseQueryParams,
  ): Promise<T[]> {
    const options: FindManyOptions<T> = {
      where: filter,
      skip: params?.offset || undefined,
      take: params?.limit || undefined,
      order: {
        createdAt: 'ASC', // Або 'DESC', якщо потрібно сортування в зворотному порядку
      } as FindOptionsOrder<T>,
    };

    return this.repository.find(options);
  }

  async count(filter?: FindOptionsWhere<T>): Promise<number> {
    const options: FindManyOptions<T> = {
      where: filter,
    };

    return this.repository.count(options);
  }

  async findOne(id: string): Promise<T> {
    return this.repository.findOne({
      where: {
        id,
      } as FindOptionsWhere<T>,
    });
  }

  async create(entity: DeepPartial<T>): Promise<T> {
    const newEntity = this.repository.create(entity);
    return this.repository.save(newEntity);
  }

  async update(id: string, updateEntityDto: Partial<T>): Promise<T> {
    await this.repository.update(id, {
      ...updateEntityDto,
    } as QueryDeepPartialEntity<T>);
    return this.repository.findOne({
      where: {
        id,
      } as FindOptionsWhere<T>,
    });
  }

  async remove(id: string): Promise<void> {
    await this.repository.delete(id);
  }
}
