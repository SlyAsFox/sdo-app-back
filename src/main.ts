import { ValidationPipe, VersioningType, Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import 'reflect-metadata';
import { AllExceptionFilter } from "./base";
import * as session from 'express-session';
import { ConfigService } from '@nestjs/config';
import { AdminJSOptions } from './admin-js-options.interface';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);

  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
    }),
  );

  app.enableVersioning({
    type: VersioningType.URI,
  });

  const config = new DocumentBuilder()
    .setTitle('SDO App API')
    .setDescription('Internal Holders API for developers.')
    .setVersion(process.env.VERSION)
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'Authorization',
    )
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/api/docs', app, document);

  app.useGlobalFilters(new AllExceptionFilter());

  app.enableCors({
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  });

  const { default: AdminJS } = await import('adminjs');
  const { default: AdminJSExpress } = await import('@adminjs/express');

  const adminJsOptions = app.get<AdminJSOptions>('ADMIN_JS_OPTIONS');

  const adminJs = new AdminJS(adminJsOptions.adminJsOptions);
  const router = AdminJSExpress.buildAuthenticatedRouter(
    adminJs,
    adminJsOptions.auth,
    null,
    adminJsOptions.sessionOptions,
  );

  app.use(adminJsOptions.adminJsOptions.rootPath, router);

  const port = process.env.PORT || 3000;
  await app.listen(port);

  Logger.log(
    `🚀 Application is running on: http://localhost:${port}/${globalPrefix}`,
  );
}

bootstrap();
