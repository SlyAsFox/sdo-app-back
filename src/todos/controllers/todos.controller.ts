import {
  Controller,
  Post,
  Param,
  Body,
  Delete,
  Patch,
  NotFoundException,
  ForbiddenException,
} from '@nestjs/common';
import { TodosService } from '../services';
import { Todo } from '../entities';
import { CreateTodoDto, UpdateTodoDto } from '../dto';
import { CurrentUser } from '../../auth/decorators';

@Controller({
  version: '1',
  path: 'todos',
})
export class TodosController {
  constructor(private readonly todosService: TodosService) {}

  @Post()
  async create(
    @CurrentUser('sub') userId: string,
    @Body() createUserDto: CreateTodoDto,
  ): Promise<Todo> {
    return this.todosService.create({ ...createUserDto, userId });
  }

  @Patch(':id')
  async update(
    @CurrentUser('sub') userId: string,
    @Param('id') id: string,
    @Body() updateUserDto: UpdateTodoDto,
  ): Promise<Todo> {
    const existTodo = await this.todosService.findOne(id);
    if (!existTodo) {
      throw new NotFoundException('TODO_NOT_FOUND');
    }
    if (existTodo.userId !== userId) {
      throw new ForbiddenException();
    }
    return this.todosService.update(id, updateUserDto);
  }

  @Delete(':id')
  async remove(
    @CurrentUser('sub') userId: string,
    @Param('id') id: string,
  ): Promise<void> {
    const existTodo = await this.todosService.findOne(id);
    if (!existTodo) {
      throw new NotFoundException('TODO_NOT_FOUND');
    }
    if (existTodo.userId !== userId) {
      throw new ForbiddenException();
    }
    return this.todosService.remove(id);
  }
}
