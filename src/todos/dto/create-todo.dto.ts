import { IsNotEmpty, IsString, Length, IsBoolean } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateTodoDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  @Length(1, 80)
  text: string;

  @IsNotEmpty()
  @IsBoolean()
  @ApiProperty()
  isCompleted: boolean;
}
