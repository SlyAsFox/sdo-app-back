import { IsString, IsOptional, Length, IsBoolean } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateTodoDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  @Length(1, 80)
  text: string;

  @IsOptional()
  @IsBoolean()
  @ApiPropertyOptional()
  isCompleted: boolean;
}
