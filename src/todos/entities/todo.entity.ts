import { Entity, Column, ManyToOne, BeforeInsert, BeforeUpdate } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from '../../base';
import { User } from '../../users/entities';
import * as bcrypt from 'bcrypt';

@Entity('todos')
export class Todo extends BaseEntity {
  @ApiProperty()
  @Column()
  text: string;

  @ApiProperty()
  @Column()
  userId: string;

  @ManyToOne(() => User, (user) => user.todos)
  user: User;

  @ApiProperty()
  @Column({ default: false })
  isCompleted: boolean;

  @Column({ type: 'timestamp', name: 'finished_at', default: null, nullable: true })
  finishedAt: Date;

  @BeforeInsert()
  @BeforeUpdate()
  async setFinishedAt() {
    if (this.isCompleted && !this.finishedAt) {
      this.finishedAt = new Date();
    }
  }
}
