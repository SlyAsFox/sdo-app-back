import { Module } from '@nestjs/common';
import { Todo } from './entities';
import { TodosController } from './controllers';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TodosService } from './services';

@Module({
  imports: [TypeOrmModule.forFeature([Todo])],
  controllers: [TodosController],
  providers: [TodosService],
  exports: [TodosService],
})
export class TodosModule {}
