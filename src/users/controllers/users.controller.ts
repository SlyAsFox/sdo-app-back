import {
  Controller,
  Get,
  Post,
  Param,
  Body,
  Delete,
  Patch,
  Query,
  ParseUUIDPipe,
} from '@nestjs/common';
import { UsersService } from '../services';
import { User } from '../entities';
import { CreateUserDto, UpdateUserDto } from '../dto';
import { CurrentUser } from '../../auth/decorators';
import { TodosService } from '../../todos/services';
import { Todo } from '../../todos/entities';
import { BaseQueryParams } from '../../base/dto';
import { Not } from 'typeorm';
import { ListWithCount } from '../../base/interfaces';

@Controller({
  version: '1',
  path: 'users',
})
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private todosService: TodosService,
  ) {}

  @Get()
  async findAll(
    @Query() params: BaseQueryParams,
    @CurrentUser('sub') userId: string,
  ): Promise<ListWithCount<User>> {
    const count = await this.usersService.count({ id: Not(userId) });
    const data = await this.usersService.findAll({ id: Not(userId) }, params);
    return {
      count,
      data,
    };
  }

  @Get('profile')
  async getProfile(@CurrentUser('sub') userId: string): Promise<any> {
    return this.usersService.findOne(userId);
  }

  @Patch('profile')
  async updateProfile(
    @CurrentUser('sub') userId: string,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<User> {
    return this.usersService.update(userId, updateUserDto);
  }

  @Get('email/:email')
  async findUserByUsername(@Param('email') email: string): Promise<User> {
    return this.usersService.findUserByUsername(email);
  }

  @Get(':id/todos')
  async getTodos(@Param('id') id: string): Promise<Todo[]> {
    return this.todosService.findAll({ userId: id });
  }

  @Get(':id')
  async findOne(@Param('id', ParseUUIDPipe) id: string): Promise<User> {
    return this.usersService.findOne(id);
  }

  @Post()
  async create(@Body() createUserDto: CreateUserDto): Promise<User> {
    return this.usersService.create(createUserDto);
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<User> {
    return this.usersService.update(id, updateUserDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string): Promise<void> {
    return this.usersService.remove(id);
  }
}
