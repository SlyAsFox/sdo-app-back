import {
  IsNotEmpty,
  IsString,
  IsEmail,
  IsOptional,
  Length,
  MinLength,
  Matches,
  IsUrl,
} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class CreateUserDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  @Length(0, 100)
  name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  @Length(0, 100)
  title: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  @Length(0, 100)
  location: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  @Length(0, 600)
  description?: string;

  // @IsOptional()
  // @IsString()
  // @ApiProperty({
  //   example: '+380112223344',
  //   description: 'Should be valid phone format',
  //   required: false,
  // })
  // @Matches(/^\+\d{1,3}\d{6,14}$/)
  // phone?: string;

  @IsOptional()
  @IsString()
  @IsUrl()
  @ApiProperty({ required: false })
  avatarOriginal?: string;

  @IsOptional()
  @IsString()
  @IsUrl()
  @ApiProperty({ required: false })
  avatarPreview?: string;

  @IsNotEmpty()
  @IsString()
  @IsEmail()
  @Length(0, 256)
  @ApiProperty({
    example: 'mail@gmail.com',
    description: 'Should be an email',
  })
  email: string;

  @IsOptional()
  @IsString()
  @MinLength(6)
  @ApiProperty()
  // @IsStrongPassword({ message: 'Пароль недостатньо міцний' })
  password?: string;

  @IsOptional()
  @IsString()
  @IsUrl()
  @ApiPropertyOptional()
  githubUrl?: string;

  @IsOptional()
  @IsString()
  @IsUrl()
  @ApiPropertyOptional()
  linkedinUrl?: string;

  @IsOptional()
  @IsString()
  @IsUrl()
  @ApiPropertyOptional()
  instagramUrl?: string;

  @IsOptional()
  @IsString()
  @IsUrl()
  @ApiPropertyOptional()
  facebookUrl?: string;

  // @IsOptional()
  // @ApiProperty({ required: false, type: AddressPayloadDto })
  // addressPayload?: AddressPayloadDto;
}
