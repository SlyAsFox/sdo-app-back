import {
  IsString,
  IsEmail,
  IsOptional,
  Length,
  MinLength,
  Matches,
  IsUrl,
} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateUserDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  @Length(0, 100)
  name: string;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  @Length(0, 100)
  title: string;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  @Length(0, 100)
  location: string;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  @Length(0, 1200)
  description?: string;

  // @IsOptional()
  // @IsString()
  // @ApiPropertyOptional({
  //   example: '+380112223344',
  //   description: 'Should be valid phone format',
  // })
  // @Matches(/^\+\d{1,3}\d{6,14}$/)
  // phone?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  avatarOriginal?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  avatarPreview?: string;

  @IsOptional()
  @IsString()
  @IsEmail()
  @Length(0, 256)
  @ApiPropertyOptional({
    example: 'mail@gmail.com',
    description: 'Should be an email',
  })
  email: string;

  @IsOptional()
  @IsString()
  @MinLength(6)
  @ApiPropertyOptional()
  password?: string; // @IsStrongPassword({ message: 'Пароль недостатньо міцний' })

  @IsOptional()
  @IsString()
  @IsUrl()
  @ApiPropertyOptional()
  githubUrl?: string;

  @IsOptional()
  @IsString()
  @IsUrl()
  @ApiPropertyOptional()
  linkedinUrl?: string;

  @IsOptional()
  @IsString()
  @IsUrl()
  @ApiPropertyOptional()
  instagramUrl?: string;

  @IsOptional()
  @IsString()
  @IsUrl()
  @ApiPropertyOptional()
  facebookUrl?: string;

  // @IsOptional()
  // @ApiPropertyOptional({ required: false, type: AddressPayloadDto })
  // addressPayload?: AddressPayloadDto;
}
