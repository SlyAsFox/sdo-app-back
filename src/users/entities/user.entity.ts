import {
  Entity,
  Column,
  getMetadataArgsStorage,
  BeforeInsert,
  BeforeUpdate,
  OneToMany,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from '../../base';
import { Todo } from '../../todos/entities';

@Entity('users')
export class User extends BaseEntity {
  @ApiProperty()
  @Column()
  name: string;

  @ApiProperty()
  @Column({ nullable: true })
  title: string;

  @ApiProperty()
  @Column({ nullable: true })
  location: string;

  @ApiProperty()
  @Column({ nullable: true })
  description: string;

  // @ApiProperty()
  // @Column({ nullable: true })
  // phone: string;

  @ApiProperty()
  @Column({ nullable: true, name: 'avatar_original' })
  avatarOriginal: string;

  @ApiProperty()
  @Column({ nullable: true, name: 'avatar_preview' })
  avatarPreview: string;

  @ApiProperty()
  @Column({ unique: true })
  email: string;

  @ApiProperty()
  @Column({ select: false })
  password: string;

  @ApiProperty()
  @Column({ nullable: true, name: 'github_url' })
  githubUrl: string;

  @ApiProperty()
  @Column({ nullable: true, name: 'linkedin_url' })
  linkedinUrl: string;

  @ApiProperty()
  @Column({ nullable: true, name: 'instagram_url' })
  instagramUrl: string;

  @ApiProperty()
  @Column({ nullable: true, name: 'facebook_url' })
  facebookUrl: string;

  @OneToMany(() => Todo, (todo) => todo.user)
  todos: Todo[];

  static getColumnNames(): string[] {
    const columns = getMetadataArgsStorage().columns.filter((column) => {
      return column.target === User || column.target === BaseEntity;
    });
    return columns.map((column) => column.propertyName);
  }
  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    if (this.password) {
      this.password = bcrypt.hashSync(this.password, 10);
    }
  }
}
