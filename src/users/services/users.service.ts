import { Injectable } from '@nestjs/common';
import { BaseService } from '../../base';
import { User } from '../entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService extends BaseService<User> {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {
    super(userRepository);
  }

  async findUserByUsername(
    username: string,
    opts?: {
      password?: boolean;
    },
  ): Promise<User | null> {
    const entity = await this.userRepository.findOne({
      where: {
        email: username,
      },
      select: opts?.password
        ? (User.getColumnNames() as (keyof User)[])
        : undefined,
    });

    return entity || null;
  }
}
