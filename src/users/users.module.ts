import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities';
import { UsersService } from './services';
import { UsersController } from './controllers';
import { TodosModule } from '../todos/todos.module';

@Module({
  imports: [TypeOrmModule.forFeature([User]), TodosModule],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
